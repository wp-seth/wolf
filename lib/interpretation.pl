my $url_stackexchange = 'https://tex.stackexchange.com/questions/';
my $err_re = qr/(?:!|[^:\n]+\.[^:\n]+:[0-9]+:)/;

{	'ltx' => [
		{
			're' => qr/Undefined control sequence\.\nl\.(?<line>\d+) (?:\.\.\..*|)(?<command>\\\S+)/,
			'interpretation' => "command '\$+{command}' in line \$+{line} is unknown. "
				. "maybe the command is misspelled or you didn't load the right package.",
		},{
			're' => qr/^(?<filename>[^:\n]+\.[^:\n]+):(?<line>[0-9]+): Undefined control sequence\.\n.*?\.\.\..*(?<command>\\\S+)/,
			'pre-proc' => sub{
				my $self = shift;
				my $text_ref = shift;
				my $plus = shift;
				$plus->{'filename'} = $self->{'orig_ltx'} if $plus->{'filename'} =~ /\Q$self->{'tmp_ltx_f'}\E$/;
			},
			'interpretation' => "command '\$+{command}' in line \$+{line} of file '\$+{filename}' is unknown.\n"
				. "maybe the command is misspelled or you didn't load the right package.",
		},{
			're' => qr/^(?:$err_re Missing \$ inserted.\s+(?:.*\s+){1,3}l.|! Extra \}, or forgotten \$.\s+.*\s+l.)(?<line>\d+)/,
			'interpretation' => 'probably you forgot a dollar-sign ($) somewhere around line $+{line}.',
		},{
			're' => qr/^$err_re Argument of \\\@\Qcaption has an extra \E\}\..*\s+\\par/s,
			'interpretation' => "maybe you should use '\\newline' instead of '\\\\'.",
		},{
			're' => qr/^$err_re (?:LaTeX Error: )?Float\(s\) lost\./,
			'interpretation' => '1. The most likely reason is that you placed a float or a \\marginpar '
				. 'command inside another float or marginpar, or inside a minipage environment, a \\parbox '
				. 'or \\footnote. Note that the error may be detected a long way from the problematic '
				. 'command(s), so the techniques of tracking down elusive errors all need to be called '
				. "into play. (source: https://www.tex.ac.uk/FAQ-fllost.html)\n"
				. '2. maybe you placed a \\todo inside a \\footnote?',
		},{
			're' => qr/^$err_re (?:LaTeX Error: )?Unknown float option/,
			'interpretation' => 'possible float options normally are h, t, b, p (here, top, bottom, page), see '
				. $url_stackexchange . '39017/'
				. 'how-to-influence-the-position-of-float-environments-like-figure-and-table-in-lat'
				. ' for details.',
		},{
			're' => qr/^$err_re (?:LaTeX Error: )?File `(?<packagename>[^']+)\.sty' not found\./,
			'interpretation' => 'you tried to load a package called $+{packagename} whch is not installed yet. '
				. 'maybe you misspelled the package. or you should install it. '
				. 'or maybe there is an alternative package.',
		},{
			're' => qr/^LaTeX\sFont\sWarning:\s
				Font\sshape\s.O[A-Z0-9]{2}\/cm(?:[a-z]+\/)+[a-z]+'\s
				in\ssize\s<.*>\snot\savailable/sx,
			'interpretation' => "try inserting '\\RequirePackage{fix-cm}' "
				. "_before_ \\documentclass{...}, see "
				. "<" . $url_stackexchange . "4824/what-do-these-font-shape-warnings-mean>",
		},{
			're' => qr/LaTeX Warning: Marginpar on page [0-9]+ moved\./s,
			'interpretation' => "sometimes \\todo (from todonotes package) makes problems. "
				. "maybe you placed a \\todo and a \\label inside of a \\caption? "
				. "this warning can be avoided in many cases "
				. "by using \\todo[inline] instead of pure \\todo.",
			# see also https://tex.stackexchange.com/questions/23977/what-does-the-warning-about-moved-marginpar-mean
		},{
			're' => qr/Package amsmath Warning: Unable to redefine math accent \\vec/s,
			'interpretation' => "try inserting '\\RequirePackage[cmex10]{amsmath}' "
				. "_before_ \\documentclass{...}, see "
				. "<" . $url_stackexchange . "102507/"
				. "how-to-fix-this-amsmath-warning-about-redefining-vec/251672#251672>",
		},
	],
	'bib' => [
	],
};
