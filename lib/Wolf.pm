package Wolf;
use 5.018002;
use strict;
use warnings;
use Data::Dumper;  # Dumper
use Date::Format qw(time2str);
use File::Compare; # compare
use File::Copy;    # copy
use File::Slurp qw/read_dir slurp/;
use File::Spec;
use FindBin;
use List::MoreUtils qw(uniq);
use POSIX qw(strftime);
use Term::ANSIColor;
use Tie::IxHash;   # ordered hashes

use Exporter ();
our @ISA = qw(Exporter);
our %EXPORT_TAGS = ( ); # eg: TAG => [ qw!name1 name2! ],
# exported package globals, as well as any optionally exported functions
our @EXPORT_OK = qw();
our $VERSION = 2.0.0; # 2020-08-01

sub new{
	my $class = shift;
	my $params = shift;
	my $self = bless {
		'biber'           => 0, # if 1 use biber
		'color-def'       => {
			'err'            => 'bold white on_red',
			'warn'           => 'bold yellow on_black',
			'notice'         => 'yellow on_black',
			'std_suspicious' => 'white on_black',
		},
		'colors'          => $params->{'colors'},
		'format-output'   => $params->{'format-output'},
		'files-not-found' => [],
		'latex_command'   => undef, # will be set later
		'latex_compiler'  => $params->{'latex_compiler'} // 'pdflatex',
		'latexfile'       => $params->{'latexfile'}, # .tex extension will be deleted
		'max_line_len'    => 73, # maximum line length in latex log file
		'no_cont'         => $params->{'no_cont'} // 0, # if 1 then exit after compile
		'new_bbl'         => undef, # see sub changed_bbl
		'old_bbl'         => undef, # see sub changed_bbl
		'orig_btx'        => {}, # see latex_compile
		'orig_ltx'        => undef, # orig latex file (with .tex extension), set later
		'exit_value'      => 0,
		'interpretations' => undef,
		'old_glossary'    => '', # backup content of glossary file
		'pdflatex-output' => $params->{'pdflatex-output'},
		'pdfviewer'       => undef,
		'tmp_ltx'         => 'temp_latex_file' . time() .
			sprintf('%d', 1_000_000 * rand()),
		'tmp_ltx_f'       => undef, # tmp latex file (with .tex extension), set later
		'unlink_files'    => $params->{'unlink_files'} // [],
		'usedfiles'       => undef, # will be set in set_latexfile
		'write18_used'    => undef, # will be set in extract_usedfiles
		'verbosity'       => $params->{'verbosity'} // 1,
	}, $class;
	for my $p('', '../'){ # in order to work with the tests
		my $path = "$FindBin::RealBin/${p}lib/interpretation.pl";
		$self->{'interpretations'} = do($path) if -e $path;
	}
	if($self->set_latexfile($params->{'latexfile'})){ # set $self->{'tmp_ltx_f'}
		$self->{'latex_command'} = $self->{'latex_compiler'} . " -recorder -file-line-error "
			. ($params->{'shell-restricted'} ? '-shell-restricted' :
				$params->{'shell-escape'} ? '-shell-escape' : '-no-shell-escape')
			.	" -interaction=nonstopmode --halt-on-error $self->{'tmp_ltx_f'}";
	}
	# pdf viewer
	my $status_pdfviewer = $self->load_pdf_viewer($params->{'pdfviewers'});
	if($status_pdfviewer eq 'none'){
		$self->msg(1, 'as you wish, no pdf viewer will be loaded.');
	}elsif($status_pdfviewer eq 'no viewer found'){
		$self->msg(1, 'couldn\'t load any of the given pdf viewers (' .
			join(', ', @{$params->{'pdfviewers'}}) . ').', 'warning');
		sleep 3;
	}
	return $self;
}

sub changed_bbl{
	my $self         = shift;
	my $changed      = 1;
	# if biber is used, this indicator will not work
	unless($self->{'biber'} == 1){
		# tmp bbl file is used for checking whether bibtex should be called
		my $bbl = $self->{'tmp_ltx'} . '.bbl';
		if(-e $bbl){
			$self->{'new_bbl'} = slurp($bbl);
			if(defined $self->{'old_bbl'}){
				$changed = undef if $self->{'old_bbl'} eq $self->{'new_bbl'};
				if(defined $changed){
					$self->msg(1, 'bbl file has changed.')
				}else{
					$self->msg(1, 'bbl file has not changed. probably no need to re-run ' .
						'latex compiler');
				}
			}else{
				$self->msg(2, 'old bbl content not found, creating...');
			}
			$self->{'old_bbl'} = $self->{'new_bbl'};
		}else{
			$self->msg(0, 'could not load bbl file', 'warning');
		}
	}
	return $changed;
}

sub changed_orig_files{
	my $self    = shift;
	my $last_change_time_bak = shift;
	my $last_change_time     = shift;
	my $changed = undef;
	if(compare($self->{'orig_ltx'}, $self->{'tmp_ltx_f'})){
		$changed = 1;
		$self->msg(3, 'main tex file has changed');
	}elsif($last_change_time_bak < $last_change_time){
		$changed = 1;
		$self->msg(3, 'last change time has changed');
	}elsif(`find . -newer $self->{'tmp_ltx_f'}` =~
		/\.(?:cls|sty)$|\/gfx\/.*\.(?:eps|p[dg]f|png|tex)\n/m){
		$changed = 1;
		$self->msg(3, 'some used file has changed');
	}
	return $changed;
}

sub check_bib_for_update{
	my $self     = shift;
	my $orig_btx = shift // [keys %{$self->{'orig_btx'}}];
	my $bib_call_required = 0;
	for my $bib(@$orig_btx){
		if(-f $bib){ # if bibtex files exist and have been changed
			if(compare($bib, $self->filename2temp($bib))){
				$bib_call_required = 1;
				$self->msg(2, "bib file '$bib' has changed");
			}
		}else{
			$self->msg(0, "file '$bib' not found", 'error');
			sleep 1;
		}
	}
	return $bib_call_required;
}

sub check_for_pdf_annotations{
	my $self      = shift;
	my $pdf_file  = shift;
	my ($exist_anno_file, $exist_anno, $infostring) =
		$self->get_pdf_viewer_annotations($pdf_file);
	if($exist_anno_file){
		if(defined $self->{'pdfviewer'}){
			$self->msg(1, "else");
			$self->msg(1, "====");
			$self->msg(1, "your pdfviewer: $self->{'pdfviewer'}");
		}
		if($exist_anno>0){
			$self->msg(0,
				'there seem to be annotations for this file that would be lost when ' .
					'recompiling',
				'warning');
			$self->msg(0, $infostring);
			$self->msg(0, 'press <ctrl+c> to leave, or <enter> to continue.');
			<STDIN>;
		}else{
			$self->msg(1, $infostring);
		}
	}
	return $exist_anno;
}

sub check_tex_for_update{
	my $self     = shift;
	my $last_change_time_bak = shift;
	my $last_change_time = shift;
	# check whether original source files have been changed
	my $latex_call_required = $self->changed_orig_files(
		$last_change_time_bak, $last_change_time);
	if($latex_call_required){
		$self->msg(2, "latex files changed; need to re-compile");
	}
	# check whether missing files are there now
	for my $fnf(@{$self->{'files-not-found'}}){
		if(-e $fnf){
			$self->msg(1, "found missing file '$fnf'; need to re-compile");
			$latex_call_required = 1;
			# fnf-array will be reset and rebuilt in parse_tex_log
			last;
		}
	}
	return $latex_call_required;
}

sub copyfile{
	my $self = shift;
	my $src  = shift;
	my $dest = shift;
	my $ret;
	if(-e $src){
		#$ret = `cp $src $dest`;
		$ret = copy($src, $dest)
			or $self->msg(0, "copy failed: $! '$src'", 'warning');
	}else{
		$ret = 0;
	}
	return $ret;
}

sub deliver_pdf{
	my $self     = shift;
	my $pdf_file = $self->{'latexfile'} . '.pdf'; # resulting pdf file
	# tmp pdf will be the result and will be copied over target pdf
	if(compare($self->{'tmp_ltx'} . '.pdf', $pdf_file)){
		$self->check_for_pdf_annotations($pdf_file);
		#sleep 2 if $self->{'pdfviewer'} eq 'okular';
		if(defined $self->{'pdfviewer'}
				&& $self->{'pdfviewer'} =~ /^(?:xdg-open|okular|evince)$/){
			sleep 2; # some sleep is necessary, because otherwise copying could be
			# started before reloading of pdf is finished
		}
		$self->copyfile($self->{'tmp_ltx'} . '.pdf', $pdf_file);
		return 1;
	}
	return 0;
}

sub extract_bibfiles{ # from latex file, and append extensions '.bib'
	my $self     = shift;
	my $bibfiles = {};
	my @latex_content = grep {!/^\s*%/} slurp $self->{'orig_ltx'};
	map {s/(?<!\\)(?:\\\\)*\K%.*//} @latex_content;
	my $latexcontent = join '', @latex_content;
	# \bibliography seems to add .bib, right?
	while($latexcontent =~ /\\bibliography\{(.*?)\}/g){
		map {$bibfiles->{$_ . '.bib'} = 1} split /\s*,\s*/, $1;
	}
	# \addbibresource seems to need .bib, right?
	while($latexcontent =~ /\\addbibresource\{(.*?)\}/g){
		$self->{'biber'} = 1;
		map {$bibfiles->{$_} = 1} split /\s*,\s*/, $1;
	}
	if($self->{'biber'} == 0  # vim: {
		&& $latexcontent =~ /\\usepackage(?:\[([^\]]*)\])\{[^}]+\bbiblatex\b/s
		&& $1 =~ /\bbackend\s*=\s*biber\b/){
		$self->{'biber'} = 1;
	}
	if($self->{'biber'} > 0){
		$self->msg(1, 'using biber instead of bibtex');
		unless(`which biber`){
			$self->msg(0, 'could not find biber, '
				. 'but biber seems to be needed to compile the bibliography', 'error');
			$self->{'biber'} = 0;
		}
	}
	# check paths of $BIBINPUTS if file is not in current working directory
	my @bibinputs = split /:/, `kpsewhich --expand-path '\$BIBINPUTS'`;
	my @old_paths = keys %$bibfiles;
	map {
		if(not -f $_){
			for my $path(@bibinputs){
				my $new_path = $path . '/' . $_;
				if(-f $new_path){
					delete $bibfiles->{$_};
					$bibfiles->{$new_path} = 1;
					last;
				}
			}
		}
	} @old_paths;
	return $bibfiles;
}

sub extract_bibstyle{
	my $self      = shift;
	my $used_latex_files = [grep {m/\.tex\z/} @{$self->{'usedfiles'}}];
	my $bibstyles = [];
	for my $file(@$used_latex_files){
		if(-e $file){
			my $content = slurp($file); # for vim syntax highlighting: {
			while($content =~ /\\bibliographystyle\s*\{([^}]++)\}/gs){
				push @$bibstyles, (split /,\s*/, $1);
			}
		}else{
			$self->msg(0, "could not find file '$file'", 'error');
		}
	}
	return $bibstyles;
}

sub extract_usedfiles{
	my $self        = shift;
	my $options     = shift // {};
	my %default_options = (
		'latexfile' => $self->{'orig_ltx'},
		'method'    => 'flsfile',
		'usedfiles' => [],
	);
	map {
		$options->{$_} = $default_options{$_} unless defined $options->{$_};
	} keys %default_options;
	my $errors = [];
	if($options->{'method'} eq 'flsfile'){
		my $file_prefix = $self->{'tmp_ltx'};
		@{$options->{'usedfiles'}} = slurp($file_prefix . '.fls');
		my $path = shift @{$options->{'usedfiles'}};
		$path =~ s/^PWD (.*)\n?/$1\//;
		@{$options->{'usedfiles'}} = uniq(
			grep {
				/^INPUT (?!\.\/$file_prefix)(?!$file_prefix)(.*)/ and
				$_ = substr($1, 0, 1) eq '/' ? $1 : $path . $1;
			} @{$options->{'usedfiles'}}
		);
	# method till 2013-08-16: faulty because not all files were matched
	}elsif($options->{'method'} eq 'logfile'){
		my $file_prefix = $self->{'tmp_ltx'};
		@{$options->{'usedfiles'}} = slurp($file_prefix . '.log') =~
			m/^(?:\)+ )?\(\K[.\/][^ \s)]+|^\s*File: \K[.\/][^ \s]+/gm;
	# very old method till 2012: faulty because not all files were matched
	}elsif($options->{'method'} eq 'texfile'){
		if(open(my $INFILE, '<', $options->{'latexfile'})){
			while(my $line = <$INFILE>){
				next if $line =~ /^\s*%/; # skip comments
				$self->{'write18_used'} = 1 if $line =~ /\\write18/;
				while($line =~ /\\in(?:clude(?:graphics)?|put)\s*
						(?:\[[^\]]+\]\s*)? \{(.*?)\}/gx){
					my $filename =
						-e $1.'.tex' ? $1.'.tex' :
						-e $1.'.eps' ? $1.'.eps' :
						-e $1.'.jpg' ? $1.'.jpg' :
						-e $1.'.pdf' ? $1.'.pdf' :
						-e $1.'.png' ? $1.'.png' : $1;
					unless(grep {$_ eq $filename} @{$self->{'usedfiles'}}){
						push @{$self->{'usedfiles'}}, $filename;
						push @$errors, @{$self->extract_usedfiles(
							{'latexfile' => $filename,
								'method' => 'texfile'}
						)}; # recursion!
					}
				}
			}
			close($INFILE);
		}else{
			push @$errors, "error reading file '$options->{'latexfile'}'. $!";
			#$self->msg(0, $errors->[-1]);
		}
	}else{
		push @$errors, "method '$options->{'method'}' undefined";
	}
	return $errors;
}

sub filename2temp{
	my $self     = shift;
	my $filename = shift;
	$filename =~ s/[\/*?]/_/g;
	return 'temp_'.$filename;
}

sub sys{
	my $self = shift;
	my $cmd = shift;
	my ($type) = $cmd =~ /^([^ ]+)/;
	if($self->{'verbosity'} > 1){
		print "command: $cmd\n";
	}
	my $child_pid = open(my $stdout, '-|', "$cmd 2>&1");
	if($child_pid){
		if($self->{'verbosity'} > 0){
			if($self->{'format-output'} == 0){
				while(<$stdout>){
					print $_;
				}
			}else{
				if($type =~ /^(?:pdflatex|lualatex|xelatex)\z/){
					if($self->{'pdflatex-output'}){
						$self->format_std_out_pdflatex($stdout, $type);
					}
				}else{
					# bibtex, biber or other cmds
					while(<$stdout>){
						print "$type: $_";
					}
				}
			}
		}
		waitpid $child_pid, 0;
	}else{ # no child pid -> is child
		# child should just die
		exit 0;
	}
	$self->{'exit_value'} = $?;
	$self->msg(4, 'last value: ' . $self->{'exit_value'});
	return $? == 0;
}

sub format_std_out_pdflatex{
	my $self   = shift;
	my $stdout = shift; # file handle or \n separated string
	my $compiler = shift; # pdflatex, ...
	my $indent_depth = 0;
	my $cont_line = 0; # line is going to be continued
	my $cont_state; # e.g. a class or a package mentioned in a warning
	# the last parenthesis of the previous line was a '('
	my $opening_paren_prev_line = 0;
	my $opening_brace_prev_line = 0;
	my $opening_chevron_prev_line = 0;
	my $bracket_open = 0;
	my $color;
	my $leave_space = 1;
	my $line;
	my $prev_line = "\n";
	if(ref $stdout eq 'GLOB'){
		$line = <$stdout>;
	}elsif($stdout =~ /^(.*+\n?)/g){
		$line = $1;
	}
	while(defined $line){
	#	LOOP: {
	#		print(" digits"), redo LOOP if /\G\d+\b[,.;]?\s*/gc;
	#		print(" lowercase"), redo LOOP
	#		if /\G\p{Ll}+\b[,.;]?\s*/gc;
	#		print(" UPPERCASE"), redo LOOP
	#		if /\G\p{Lu}+\b[,.;]?\s*/gc;
	#		print(" Capitalized"), redo LOOP
	#		if /\G\p{Lu}\p{Ll}+\b[,.;]?\s*/gc;
	#		print(" MiXeD"), redo LOOP if /\G\pL+\b[,.;]?\s*/gc;
	#		print(" alphanumeric"), redo LOOP
	#		if /\G[\p{Alpha}\pN]+\b[,.;]?\s*/gc;
	#		print(" line-noise"), redo LOOP if /\G\W+/gc;
	#		print ". That's all!\n";
	#	}
		$self->msg(4, "[new line '" . substr($line, 0, -1) . "']");
		my $line_len = length($line);
		my $line_bak = $line;
		my $output = '';
		my $line_was_cont = $cont_line; # line was being continued
		my $cont_state_tmp; # cont_state just for this line
		while(length($line) > 0){
			$self->msg(4, "[present line '" . substr($line, 0, -1) . "']");
			my $indent = '  ' x $indent_depth;
			# brackets [x ...], x = int
			if($bracket_open){
				$self->msg(4, '[bracket is open]');
				if($line =~ /\]/){
					$bracket_open = 0;
					--$indent_depth;
				}
				if($line =~ /^(\s*[{<][^{<>}]*)/){
					$self->msg(4, '[got < or {]'); # }
					$line = substr($line, length($1));
					$output .= $indent . $1;
					$cont_line = 1;
					$opening_paren_prev_line = 0;
				}elsif($line =~ /^([^{<>}]*[>}])/){
					$line = substr($line, length($1));
					$output .= "$1\n";
					$cont_line = 0;
					$opening_paren_prev_line = 0;
				}else{ # no [<>{}]
					$output .= $line;
					$line = '';
					$cont_line = 0;
					$opening_paren_prev_line = 0;
				}
				$line =~ s/^\s+//;
			}elsif($line =~ /^((?:[^\[]|\[[0-9]+\])*+) (\[[0-9]+)(\s*+) [{<]/x
					# } for vim
					&& length($line) > 78){
				$self->msg(4, '[got bracket and content]');
				$line = substr($line, length($1) + length($2) + length($3));
				my $pre_bracket = $1;
				my $bracket = $2;
				$output .= $indent . $pre_bracket . "\n";
				$output .= $indent . $bracket . "\n";
				++$indent_depth;
				$cont_line = 0;
				$bracket_open = 1;
				$opening_paren_prev_line = 0;
			}
			# latex warning with explicit class/package name
			elsif($line =~ /^(?<line>(?:Class|Package)\s
					(?<packageclass>[a-zA-Z0-9_-]+)\s[wW]arning.*)$/x
			){
				$cont_state_tmp = $+{'packageclass'};
				$self->msg(4, "[got latex warning of package '$cont_state_tmp']");
				$output .= $indent . $+{'line'};
				$line = '';
				$cont_line = 0;
				$opening_paren_prev_line = 0;
			}
			# general latex warning
			elsif($line =~ /^(LaTeX Warning: .*|pdfTeX warning: pdflatex .*)$/){
				$self->msg(4, '[got latex warning]');
				$output .= $indent . $1;
				$line = '';
				$cont_line = 1;
				$opening_paren_prev_line = 0;
			}
			# ... ( ... )
			elsif($line =~ /^(?<line>[^()]*+ \( (?<packageclass>[^()]*+) \))/x){
				if(defined $cont_state && $cont_state eq $+{'packageclass'}){
					$cont_state_tmp = $cont_state;
					$self->msg(4, "[got ($cont_state)]");
					$output .= $indent . $line;
					$line = '';
					$cont_line = 0;
					$opening_paren_prev_line = 0;
				}else{
					#undef $cont_state;
					$self->msg(4, '[got ()]');
					$output .= $indent if !$cont_line;
					$output .= $+{'line'} . "\n";
					$line = substr($line, length($+{'line'}));
					$cont_line = 0;
					$leave_space = 0;
					$opening_paren_prev_line = 0;
				}
			}
			# < ...
			elsif($line =~ /^(\s*+)(<\/[a-zA-Z0-9.~+\/-]++)/x){
				$self->msg(4, '[got <]');
				$output .= $indent if !$cont_line;
				$output .= "$2";
				$line = substr($line, length($1) + length($2));
				$cont_line = 1;
				$opening_chevron_prev_line = 1;
				$leave_space = 0;
				$opening_paren_prev_line = 0;
			}
			elsif($prev_line =~ /^.{78}<$/ && $line =~ /^(\/[a-zA-Z0-9.~+\/-]++)/x){
				$self->msg(4, '[continue after last line ended with <]');
				$output .= $indent if !$cont_line;
				$output .= "$1";
				$line = substr($line, length($1));
				$cont_line = 1;
				$opening_chevron_prev_line = 1;
				$leave_space = 0;
				$opening_paren_prev_line = 0;
			}
			# ... >
			elsif($opening_chevron_prev_line){
				$self->msg(4, '[expecting >]');
				if($line =~ /^([a-zA-Z0-9.~+\/-]*+>)/x){
					$self->msg(4, '[got >]');
					$output .= $indent if !$cont_line;
					$output .= "$1\n";
					$line = substr($line, length($1));
					$leave_space = 0;
					$opening_paren_prev_line = 0;
					$opening_chevron_prev_line = 0;
					$cont_line = 0;
				}elsif($line !~ /^\s*$/){
					$opening_chevron_prev_line = 0;
					$cont_line = 0;
				}else{
					$line = '';
				}
			}
			# { ...
			elsif($line =~ /^(\s*+)(\{\/[a-zA-Z0-9.~+\/-]++)/x){
				$self->msg(4, '[got {]'); # } for vim
				$output .= $indent if !$cont_line;
				$output .= "$2";
				$line = substr($line, length($1) + length($2));
				$cont_line = 1;
				$opening_brace_prev_line = 1;
				$leave_space = 0;
				$opening_paren_prev_line = 0;
			}
			# ... }
			elsif($opening_brace_prev_line){ # { for vim
				$self->msg(4, '[expecting }]');
				if($line =~ /^([a-zA-Z0-9.~+\/-]*+\})/x){ # { for vim
					$self->msg(4, '[got }]');
					$output .= $indent if !$cont_line;
					$output .= "$1\n";
					$line = substr($line, length($1));
					$leave_space = 0;
					$opening_paren_prev_line = 0;
					$opening_brace_prev_line = 0;
					$cont_line = 0;
				}elsif($line !~ /^\s*$/){
					$opening_brace_prev_line = 0;
					$cont_line = 0;
				}else{
					$line = '';
				}
			}
			# ( ...
			elsif($line =~ /^\s*+\( ( [^()]*+ )/x){
				$output .= $indent if !$cont_line;
				my $filename = $1;
				$self->msg(4, "[got (, length = " . length($filename) . "]");
				$line = substr($line, length($filename) + 1);
				++$indent_depth;
				if(length($filename) == 79 and $filename =~ /^\S+$/){
					chomp $filename;
					$output .= "($filename";
					$cont_line = 1;
					$opening_paren_prev_line = 1;
				}else{
					$output .= "($filename\n";
					$output =~ s/\n\n\z/\n/;
					$cont_line = 0;
					$opening_paren_prev_line = 0;
				}
				$leave_space = 0;
			}
			# ... )
			elsif($indent_depth > 0 and $line =~ /^([^()]*+) \)/x){
				$self->msg(4, '[got )]');
				my $prefix = $1;
				$line = substr($line, length($1) + 1);
				$line =~ s/^ +//;
				$line = '' if $line =~ /^\s+$/;
				if($prefix =~ /^\s*$/){
					$output .= "\n" if $cont_line and !$opening_paren_prev_line;
				}else{
					$output .= $indent if !$cont_line;
					$output .= $prefix;
					$output .= "\n" if !$opening_paren_prev_line;
				}
				--$indent_depth;
				$indent = '  ' x $indent_depth;
				$output .= $indent if !$opening_paren_prev_line;
				$output .= ")\n";
				$cont_line = 0;
				$opening_paren_prev_line = 0;
			}
			# { ... }
			#elsif($line =~ /^( [^()]*+ \( [^()]*+ \) [^()]*+ )/x){
			#	$self->msg(4, '[got ()]');
			#	$output .= $indent if !$cont_line;
			#	$output .= $1;
			#	$line = substr($line, length($1));
			#	$cont_line = length($line) > 0 ? 1 : 0;
			#	$opening_paren_prev_line = 0;
			#}
			else{
				if($line !~ /^\s*$/ or $leave_space){
					$output .= $indent if !$cont_line;
					$output .= $line;
					$cont_line = 0;
					# <
					if($line_len == 80 && $line =~ /^<$/){
						$self->msg(4, '[got < at end of line]');
						$cont_line = 1;
					}
					$line = '';
					$opening_paren_prev_line = 0;
				}else{
					$line = '';
				}
			}
		}
		$cont_state = $cont_state_tmp; # maybe set undef here
		$leave_space = 1;
		chomp $output;
		$output =~ s/\Q$self->{'tmp_ltx'}/$self->{'latexfile'}/g;
		if($output =~ /\b(?i:error(?!\sstyle\smessages\senabled)|warning)\b
				|(?:Overfull\s|Underfull\s)/x
			|| defined($cont_state)
		){
			$color = $self->{'color-def'}{'std_suspicious'};
		}
		if($prev_line =~ /^\s*$/ && $output =~ /^\s*$/){
			$self->msg(4, "[skip multiple empty lines]");
		}else{
			my $parsed = ref $stdout eq 'GLOB' ? $compiler . ': ' : '';
			print $parsed if $line_was_cont == 0;
			$self->msg_colored(1, '' . (join "\n$parsed", split /\n/, $output), $color);
			if($cont_line == 0){
				print "\n";
			}
		}
		if($cont_line == 0){
			undef $color;
		}
		$prev_line = $line_bak;
		if(ref $stdout eq 'GLOB'){
			$line = <$stdout>;
		}elsif($stdout =~ /\G(.*+\n?)/g){
			$line = $1;
		}else{
			undef $line;
		}
	}
}

sub get_first_found_exe{
	my $self       = shift;
	my $candidates = shift;
	my @path = File::Spec->path();
	for my $candidate(@$candidates){
		$self->msg(2, "* $candidate");
		for my $dir(@path){
			my $file = File::Spec->catfile($dir, $candidate);
			return $candidate if -x $file;
		}
	}
	return;
}

sub get_min_crossref_param{
	my $self = shift;
	my $min_crossrefs = shift;
	unless(defined $min_crossrefs){
		my $bib_styles = $self->extract_bibstyle();
		# see http://tug.ctan.org/biblio/bibtex/contrib/IEEEtran/IEEEtran_bst_HOWTO.pdf
		$self->msg(3, 'found used bibtex styles: ['. (join ', ', @$bib_styles) . ']');
		if(grep {$_ eq 'IEEEtran'} @$bib_styles){
			$self->msg(2, 'setting min-crossrefs to 900');
			$min_crossrefs = 900;
		}
	}
	my $cli_param = {
		'bibtex' => (defined $min_crossrefs ? ' --min-crossrefs=' . $min_crossrefs : '')
	};
	$cli_param->{'biber'} = $cli_param->{'bibtex'};
	$cli_param->{'biber'} =~ s/min\K-(?=crossrefs)//; # param name differs in biber
	return $cli_param;
}

sub get_pdf_viewer_annotations{
	my $self               = shift;
	my $pdf_file_full_path = shift;
	$pdf_file_full_path=~/([^\/]+)$/;
	my $pdf_file = $1;
	my $pdf_size = (stat $pdf_file_full_path)[7];
	my $numAnnotations = 0;
	my $existAnnotations = 0;
	my $info_string = '';
	my @okular_files = ();
	my @kdedirs = grep /^.kde\d*$/, read_dir($ENV{'HOME'});
	for my $kdedir(@kdedirs){
		my $path = $ENV{'HOME'} . '/' . $kdedir . '/share/apps/okular/docdata';
		if(-e $path){
			if($self->{'verbosity'} > 1){
				$info_string .= "'$path' exists, looking for '$pdf_file' xml-files.\n";
			}
			push @okular_files, grep {
				/^\d+\Q.$pdf_file.xml\E$/ and $_ = "$path/$_";
			} read_dir($path);
		}
	}
	if(@okular_files > 0 && $self->{'verbosity'} > 0){
		$info_string .= "found some okular annotations for "
			. "files with similar filename:\n";
	}
	map {
		my $filemoddate = time2str("%Y-%m-%d %T", (stat $_)[9]);
		my $filesize = (stat $_)[7];
		/\/(\d+)\Q.$pdf_file.xml\E$/;
		my $pdf_file_size_from_file_name = $1;
		if(defined $pdf_size and $pdf_file_size_from_file_name eq $pdf_size){
			++$numAnnotations if $filesize > 500;
			$existAnnotations = 1;
			$info_string .= '* ' if $self->{'verbosity'} > 0;
		}else{
			$info_string .= '  ' if $self->{'verbosity'} > 0;
		}
		$info_string .= "$filemoddate, $_ ($filesize B)\n" if $self->{'verbosity'} > 0;
	} sort {(stat $a)[9] <=> (stat $b)[9]} @okular_files;
	return ($existAnnotations, $numAnnotations, $info_string);
}

sub get_pids{
	my $self      = shift;
	my $processRE = shift;       # regular expression for processes
	my $user      = $ENV{USER};
	# the following line may cause fork problems "resource temporarily unavailable"
	#my $stdout = get_syscall_output_threaded("ps -ef | grep -v grep");
	my $stdout = `ps -ef | grep -v grep`;
	# fetch all processes of user
	my @pids = ();
	map {push @pids, $1 if /$user\s+ # user
		(\d+)\s+          # pid
		\d+\s+            # parent pid
		\d+\s+            # ?
		(?:\d\d:\d\d|[a-zA-Z]+\d+|\S+)\s+ # time
		[a-z0-9\/?]+\s+   # display
		\d\d:\d\d:\d\d\s+ # time
		.*$processRE.*    # cmd
		/x} split /\n/, $stdout;
	return \@pids;
}

sub interpret_error{
	my $self      = shift;
	my $type      = shift; # ltx_..., bib_...
	my $error     = shift;
	my $interpretation;
	my $url_stackexchange = 'https://tex.stackexchange.com/questions/';
	$type = substr($type, 0, 3);
	unless(defined $self->{'interpretations'}{$type}){
		$self->msg(0, "no interpretations defined for type '$type'", 'error');
	}
	for my $interp(@{$self->{'interpretations'}{$type}}){
		if($error =~ /$interp->{'re'}/){
			my %plus = (%+);
			$interpretation = $interp->{'interpretation'};
			if($interp->{'pre-proc'}){
				$interp->{'pre-proc'}->($self, \$interpretation, \%plus);
			}
			$interpretation =~ s/\$\+\{([^}]+)\}/$plus{$1}/ge;
			$interp->{'post-proc'}->($self, \$interpretation) if $interp->{'post-proc'};
			last;
		}
	}
	return $interpretation;
}

sub latex_compile{
	my $self        = shift;
	my $min_crossrefs = shift;
	$self->{'orig_btx'} = $self->extract_bibfiles(); # orig bibtex files
	my $other_files = [];                        # filenames of files, that are used
	for my $file(keys %{$self->{'orig_btx'}}){
		push @{$self->{'unlink_files'}}, $self->filename2temp($file);
	}
	my %compile_mode = (); # () = do nothing; other possibilities:
	# latex: compile latex (without bibtex);
	# bib: compile bibtex and latex
	# del_aux: delete aux-file
	# idx: call makeindex
	my $compiling_counter = 0;
	my $mincrossrefs = $self->get_min_crossref_param($min_crossrefs);
	my $last_change_time_bak = 0;
	$self->msg(3, 'now start looping, i.e. initial compilation and afterwards ' .
		'waiting for file changes');
	my $call_stack = []; # call stack since last file change
	while(1){ # endless loop
		my $force_error_log = 0;
		my $last_change_time = $self->newest_change($other_files);
		if($self->check_bib_for_update()){
			$compile_mode{'bib'} = 1;
			$call_stack = [];
		}
		if($compile_mode{'del_aux'}){ # in some cases the aux file should be deleted
			$self->msg(2, "deleting aux file, need to re-compile");
			push @$call_stack, ['remove aux file', time()];
			unlink "$self->{'tmp_ltx'}.aux";
			delete $compile_mode{'del_aux'};
			$compile_mode{'latex'} = 1;
		}elsif(not defined $compile_mode{'latex'} or $compile_mode{'latex'} == 0){
			$compile_mode{'latex'} = $self->check_tex_for_update(
				$last_change_time_bak, $last_change_time);
			$call_stack = [] if $compile_mode{'latex'};
		}
		if($compile_mode{'bib'} || $compile_mode{'latex'}){ # compile and use bibtex
			$force_error_log = $self->pdflatex_and_bib(
				undef, $mincrossrefs, $compile_mode{'bib'}, \$compile_mode{'latex'},
				$call_stack);
			$self->msg(3, "compile mode: " . Dumper(\%compile_mode));
		}
		if($compile_mode{'idx'}){
			push @$call_stack, ['makeindex', time()];
			$self->sys("makeindex $self->{'tmp_ltx'}");
			my $idx_state = $self->parse_makeindex_log();
			if($idx_state ne 'nothing written'){
				$compile_mode{'latex'} = 1;
			}else{
				$self->msg(2, 'no need to re-run ' . $self->{'latex_compiler'}, 'notice');
				$compile_mode{'idx'} = 0;
			}
		}
		if($compile_mode{'glo'}){
			my @params = (
				"-t $self->{'tmp_ltx'}.glg",
				"-o $self->{'tmp_ltx'}.gls",
				   "$self->{'tmp_ltx'}.glo",
			);
			unshift @params, "-s $self->{'tmp_ltx'}.ist" if -e "$self->{'tmp_ltx'}.ist";
			push @$call_stack, ['makeindex for glossary', time()];
			$self->sys('makeindex ' . join(' ', @params));
			$compile_mode{'glo'} = 0;
			$compile_mode{'latex'} = 1;
		}
		# check again for changed
		my $check_again = $self->check_tex_for_update(
			$last_change_time_bak, $last_change_time);
		$call_stack = [] if $check_again;
		if($compile_mode{'latex'} || $check_again){
			# if latex file or related files have been changed, then compile w/o using
			#  bibtex
			# copy orig file to tmp file if not done already
			unless($compile_mode{'bib'}){
				$self->copyfile($self->{'orig_ltx'}, $self->{'tmp_ltx_f'});
			}
			# after bibtex has been used, actually pdflatex has to be called twice
			# to avoid citation warnings. but as the log file is searched for
			# undefined citations, this second call should be unnecessary.
			push @$call_stack, [$self->{'latex_compiler'}, time()];
			$self->sys($self->{'latex_command'});
			# && latex -shell-escape -interaction=nonstopmode --halt-on-error
			# $self->{'orig_ltx'} && dvips $self->{'latexfile'}.dvi
			# -o $self->{'latexfile'}.ps && ps2pdf $self->{'latexfile'}.ps
		}else{ # else do nothing
			$compiling_counter = 0;
			last if $self->{'no_cont'};
			if($force_error_log){
				my $extract_errors = $self->extract_usedfiles();
				$self->parse_tex_log($extract_errors);
			}
			$self->msg(5, 'now sleep 1 and goto loop begin');
			sleep 1;
			next;
		}
		# get filenames of files that are used
		$other_files = [];
		my $extract_errors = $self->extract_usedfiles({
			'usedfiles' => $other_files,
		});
		$last_change_time_bak = $self->newest_change($other_files);
		%compile_mode = $self->parse_tex_log($extract_errors);
		my $compilation_needed = 0 < grep {defined $_ && $_ > 0} values(%compile_mode);
		if(++$compiling_counter > 2 && $compilation_needed){
			$self->msg(0, 'halt loop for there seem to persist errors/warnings.',
				'notice');
			%compile_mode = ();
		}
		$self->deliver_pdf();
		$self->print_cmd_callstack($call_stack);
		$self->msg(1, '' . strftime("%Y-%m-%d %H:%M:%S UTC", gmtime()));
	} # "end" of endless loop
	return 1; # useless, i know
}

sub load_latex_editor{
	my $self          = shift;
	my $files         = [grep {m/\.tex\z/} @{$self->{'usedfiles'}}];
	my $latex_editors_string = shift;
	my $latex_editors = [split /\|/, $latex_editors_string];
	if(!defined($latex_editors_string) || @$latex_editors == 0
		|| $latex_editors->[0] eq 'none'
	){
		return;
	}
	$self->msg(2, 'search for latex editor');
	my $latex_editor = $self->get_first_found_exe($latex_editors);
	my $pids = $self->get_pids(qr/\Q$latex_editor\E .*\Q$files->[0]\E\b/);
	# load if not loaded already
	if(@$pids == 0){
		$self->msg(1, "loading latex editor '$latex_editor'");
		my $latex_editor_cmd = $latex_editor;
		$latex_editor_cmd .= ' -p' if $latex_editor =~ /^g?vim\z/;
		my $cmd = 'LD_LIBRARY_PATH= ' . $latex_editor_cmd . ' ' . (join ' ', @$files)
			. ' &';
		$self->msg(2, $cmd);
		system($cmd);
	}else{
		$self->msg(1, "latex editor '$latex_editor' loaded already");
	}
	return $latex_editor;
}

sub load_pdf_viewer{
	my $self = shift;
	my $pdfviewers = shift;
	if(!defined($pdfviewers) || @$pdfviewers == 0 || $pdfviewers->[0] eq 'none'){
		return 'none';
	}
	$self->msg(2, 'search for pdf viewer');
	$self->{'pdfviewer'} = $self->get_first_found_exe($pdfviewers);
	return 'no viewer found' unless defined $self->{'pdfviewer'};
	unless(defined $self->{'latexfile'}){
		$self->msg(0, 'please set latex file first', 'warning');
		return 'no file set';
	}
	my $file = $self->{'latexfile'} . '.pdf';
	my $pids = $self->get_pids(qr/\Q$self->{'pdfviewer'}\E .*\Q$file\E\b$/);
	# load if not loaded already
	if(@$pids == 0){
		$self->msg(1, "loading pdf viewer '$self->{'pdfviewer'}'");
		$SIG{'CHLD'} = 'IGNORE'; # avoid creating zombie child
		my $pid = fork();
		defined $pid or die "fork failed; $!\n";
		if($pid == 0){
			unless(-e $file){
				$self->msg(1, "file $file does not exist, waiting until it exists.");
			}
			while(not -e $file){
				sleep 1;
			}
			system("$self->{'pdfviewer'} $file &");
			exit $?;
		}
		return 'loading';
	}else{
		$self->msg(1, "pdf viewer '$self->{'pdfviewer'}' loaded already");
		return 'loaded already';
	}
}

sub msg{
	my $self           = shift;
	my $verb_threshold = shift;
	my $msg            = shift;
	my $type           = shift;
	my $caller_inc     = shift // 0;
	return 0 if $self->{'verbosity'} < $verb_threshold;
	$type = (defined $type ? "$type in ": '');
	my $timestamp = strftime("%Y-%m-%d %H:%M:%S", gmtime());
	# my ($package, $filename, $line, $subr, $has_args, $wantarray, $evaltext,
	# $is_require, $hints, $bitmask, $hinthash) = caller(0);
	my @callers = caller(0 + $caller_inc);
	my $line = $callers[2];
	@callers = caller(1 + $caller_inc);
	my $subr = $callers[3] // '[no sub]';
	if($self->{'verbosity'} > 1){
		print "$timestamp $type$subr:$line: ";
	}else{
		print "wolf: ";
	}
	print "$msg\n";
	return 1;
}

sub msg_colored{
	my $self           = shift;
	my $verb_threshold = shift;
	my $msg            = shift;
	my $colors         = shift;
	return 0 if $self->{'verbosity'} < $verb_threshold;
	if($self->{'colors'} and defined $colors){
		print colored [$colors], $msg;
	}else{
		print $msg;
	}
	return 1;
}

sub newest_change{
	my $self  = shift;
	my $files = shift;
	my $max_date = 0;
	my $newest_file;
	for my $file(@$files){
		next unless -e $file;
		my $mod_ts = (stat $file)[9];
		if($mod_ts > $max_date){
			$max_date = $mod_ts;
			$newest_file = $file;
		}
	}
	if(defined $newest_file){
		$self->msg(3, "newest file: '$newest_file', timestamp = $max_date");
	}
	return $max_date;
}

sub ordered_hash_ref{
	my $self = shift;
	tie my %hash, 'Tie::IxHash', @_;
	return \%hash;
}

sub parse_makeindex_log{
	my $self = shift;
	my $idx_state = '';
	my $tmp_idx_log = $self->{'tmp_ltx'} . '.ilg'; # used for error parsing
	my $log = slurp($tmp_idx_log);
	if($log =~ /Nothing written in $self->{'tmp_ltx'}\.ind\./){
		$idx_state = 'nothing written';
	}
	return $idx_state;
}

sub parse_tex_log_bib_err{
	# bibtex and biber errors
	my $self       = shift;
	my $biblog_ref = shift;
	my $msg        = shift;
	if(defined $$biblog_ref){
		while($$biblog_ref =~ /^.*\berrors?\b.*(?:\n---.*(?:\n :.*)*)?/mgp){
			next if ${^MATCH} =~ /^\(There was (\d+) error messages?\)/;
			++$msg->{'occs'}->{${^MATCH}};
		}
	}
	$self->print_tex_log_messages(
		$msg->{'caption'}, $msg->{'occs'}, $self->{'color-def'}{'err'});
	return 1;
}

sub parse_tex_log_bib_warn{
	# bibtex and biber warnings
	my $self       = shift;
	my $biblog_ref = shift;
	my $msg        = shift;
	if(defined $$biblog_ref){
		# biber
		while($$biblog_ref =~ / WARN - .*/mgp){
			++$msg->{'occs'}->{${^MATCH}};
		}
		# bibtex
		while($$biblog_ref =~ /^Warning--.*|I found no \\[a-z]+ command---while .*/mgp){
			++$msg->{'occs'}->{${^MATCH}};
		}
	}
	$self->print_tex_log_messages(
		$msg->{'caption'}, $msg->{'occs'}, $self->{'color-def'}{'warn'});
	return 1;
}

sub parse_tex_log_check_bib_rerun{
	my $self    = shift;
	my $log_ref = shift;
	my $bib_call_required = 0;
	# cases, where bibtex should be used (again)
	if($$log_ref =~ /(?:LaTeX|Package\snatbib)\s
		Warning:\sCitation\s.*\son\spage\s\d+\sundefined\son\sinput\sline/x
		# or $log =~ /Package natbib Warning: There were undefined citations/
	){
		$self->msg(2, 'citation undefined, so re-bibtexing');
		$bib_call_required = 1;
	}
	return $bib_call_required;
}

sub parse_tex_log_check_rerun{
	my $self    = shift;
	my $log_ref = shift;
	my $latex_call_required = 0;
	# cases, where latex-rerun should help
	my @indicators = (
		'LaTeX Warning: Label(s) may have changed. Rerun to get cross-references right.',
		'Package biblatex Warning: Please rerun LaTeX.',
		'Package transparent Warning: Rerun to get transparencies right.',
		'Package rerunfilecheck Warning: File ',
	);
	if((grep {$$log_ref =~ /\Q$_/} @indicators) || $$log_ref =~ /
		Package\slineno\sWarning:\sLinenumber\sreference\sfailed,\s*\n\s*
			\(lineno\)\s*rerun\sto\sget\sit\sright\.|
		Package\snatbib\sWarning:\sCitation\(s\)\smay\shave\schanged.\s*\n\s*
			\(natbib\)\s*Rerun\sto\sget\scitations\scorrect\.|
		pdfTeX\swarning\s\(dest\):\s # for vim syntax highlighting {
			name\{[^}]+\}\shas\sbeen\sreferenced\sbut\sdoes\snot/x
	){
		$self->msg(2, 'found demand on re-compiling');
		$latex_call_required = 1;
	}
	return $latex_call_required;
}

sub parse_tex_log_boxes{
	# over-/underfull boxes
	my $self    = shift;
	my $log_ref = shift;
	my $msg     = shift;
	while($$log_ref =~ /^(?:Over|Under)full .*box.*(?:\n\[\]\\OT1\/.*)?/mgp){
		++$msg->{'occs'}->{${^MATCH}};
	}
	$self->print_tex_log_messages(
		$msg->{'caption'}, $msg->{'occs'}, $self->{'color-def'}{'notice'});
	return 1;
}

sub parse_tex_log_hyphenation{
	# hyphenation problems
	my $self    = shift;
	my $log_ref = shift;
	my $msg     = shift;  # `grep -iA 2 -E "^\\\\T1/" $tmp_ltx_log`;
	while($$log_ref =~ /^(?:\\|\[\]\\O)T1(?:.*+[\n\r]+){2}.*/mgp){
		++$msg->{'occs'}->{${^MATCH}};
	}
	$self->print_tex_log_messages(
		$msg->{'caption'}, $msg->{'occs'}, $self->{'color-def'}{'notice'});
	return 1;
}

sub parse_tex_log_err{
	my $self    = shift;
	my $log_ref = shift;
	my $msg     = shift;
	my $compile_mode = shift;
	my $re_infwarerr = qr/Package:\sinfwarerr\s.*\sProviding\sinfo\/warning\/
			(?:error\s)?messages?\s\(HO\)/x;
	# `grep -iA 2 -B 1 -E "^!" $tmp_ltx_log`.`grep -iA 1 -B 1 error $tmp_ltx_log`;
	while($$log_ref =~ /^(?:
			Package\s([a-zA-Z0-9_-]+)\s[eE]rror.*(?:\(\1\).*+\s*)*| # package errors
			!(?:.*\s+){1,3}l\.\d+(?-s:.*)|             # normal errors indicating a line
			!(?:.*\s+){2}.*|                           # normal errors
			[^:\n]+\.[^:\n]+:[0-9]+:\x20(?:.+\n){1,3}| # file:line: errors
			.*\berror\b.*+\s*.*                        # other errors
		)/mgxp){
		my $err = ${^MATCH};
		$err =~ s/[\n\r]+$//;
		# check for false positives
		next if $err =~ /$re_infwarerr/;
		next if $err =~ /file:line:error style messages enabled/;
		next if $err =~ /^Package: silence .* filtering of warnings and error m/;
		# collect unfindable files
		if($err =~ /^(.*\sError:\sFile\s`)([^']+)'\snot\sfound\b/s){
			my $fnf_pre = $1;
			my $fnf = $2;
			#print "$fnf_pre$fnf\n";
			$fnf =~ s/^(.*)\n/$1
				. ' 'x($self->{'max_line_len'} - length($fnf_pre . $1))/egm;
			if(0 == grep {$fnf eq $_} @{$self->{'files-not-found'}}){
				push @{$self->{'files-not-found'}}, $fnf;
			}
		}
		if($$log_ref =~ /\G\s*See\sthe\sLaTeX\smanual\s.*\sfor\sexplanation\.\s+
			Type\s+H\s<return>\s+for\simmediate\shelp.\s+\.\.\.\s+\n
			(l\.[0-9]+\s.*)/gpcx
		){
			$err .= "\n$1";
		}
		if($err =~ /Type X to quit or <RETURN> to proceed/){
			$err =~ s/\s*Type X to quit or <RETURN> to proceed,\s*or enter new .*\s*//;
			if($$log_ref =~ /\G\s*Enter file name:\s*(\S.*)/gpc){
				$err .= "\n$1";
			}
		}
		++$msg->{'occs'}->{$err};
		# if bbl-error, then bibtex should be used again
		if($err =~ /\Q$self->{'tmp_ltx'}.bbl/){
			$compile_mode->{'bib'} = 1;
		}
	}
	$self->print_tex_log_messages(
		$msg->{'caption'}, $msg->{'occs'}, $self->{'color-def'}{'err'});
	return 1;
}

sub parse_tex_log_warn{
	my $self    = shift;
	my $log_ref = shift;
	my $msg     = shift;  # `grep -iA 1 warning $tmp_ltx_log`;
	my $re_infwarerr = qr/Package:\sinfwarerr\s.*\sProviding\sinfo\/warning\/
			(?:error\s)?messages?\s\(HO\)/x;
	while($$log_ref =~ /^
		(?:(?:Class|Package)\s([a-zA-Z0-9_-]+)\s[wW]arning.*(?:\(\1\).*+[\n\r]*)*|
		.*(?i:warning).*+[\n\r]*.*)
	/xmgp){
		my $package = $1;
		next if ${^MATCH} =~ /$re_infwarerr/;
		next if ${^MATCH} =~ /
			^Package:\ssilence\s.*\sfiltering\sof\swarnings\sand\serror\sm|
			\\sl\@(?:BankOf)?Warning|
			LaTeX\sInfo:\sRedefining\s\\GenericWarning|
			Package\sscrlayer-scrpage\sInfo:\sdeactivating\swarning\sfor\sfont\selement
		/x;
		my $w = ${^MATCH};
		$w =~ s/\n\n(?:(?:Class|Package) .*|[\s()]*$)//s;
		# maybe warnings continues on next line(s)
		# space or paranthesis on next line indicate that the warning continues on
		# next line
		if($$log_ref =~ /\G(?:\n\x20.+)+/mgcp){ # and $w =~/^Package subfig Warning:/
			$w .= join ' ', split / *\n */, ${^MATCH};
		}elsif(defined $package && $$log_ref =~ /\G(?:\n\($package\).+)+/mgcp){
			$w .= ${^MATCH};
		}
		# comma and colon indicate that the warning continues on next line
		while($w =~ /[,:]\s*$/){
			$$log_ref =~ /\G\n.*$/mgp;
			$w .= ${^MATCH};
		}
		# collect unfindable files
		if($w =~ /LaTeX Warning: File `([^']+)' not found/
			&& 0 == grep {$1 eq $_} @{$self->{'files-not-found'}}
		){
			push @{$self->{'files-not-found'}}, $1;
		}
		++$msg->{'occs'}->{$w};
	}
	$self->print_tex_log_messages(
		$msg->{'caption'}, $msg->{'occs'}, $self->{'color-def'}{'warn'});
	return 1;
}

sub parse_tex_log{
	my $self           = shift;
	my $extract_errors = shift;
	my $log = slurp($self->{'tmp_ltx'} . '.log');
	my %compile_mode = (
		'latex' => $self->parse_tex_log_check_rerun(\$log),
		'bib' => $self->parse_tex_log_check_bib_rerun(\$log),
	);
	# cases, where aux-file should be deleted
	if($log =~ /! ?Package babel Error: You haven't loaded the language/){
		$compile_mode{'del_aux'} = 1;
	}
	# use makeindex if necessary
	if($log =~ /Writing\sindex\sfile\s(.*+)\s(.*)/x){
		my $filename_and_remainder = $1 . $2;
		if($filename_and_remainder =~ /^$self->{'tmp_ltx'}\.idx\b/){
			$compile_mode{idx} = 1;
		}
	}
	my $tmp_biblatex_log = $self->{'tmp_ltx'} . '.bcf'; # used for getting bibtex files
	# cases, where biber should be used
	if($log =~ /Package\sbiblatex\sWarning:\sPlease\s\(re\)run\sBiber\son/x){
		$self->msg(2, 'demand for biber');
		$self->{'biber'} = 1;
		$compile_mode{'bib'} = 1;
		if(-f $tmp_biblatex_log){
			my $bl_log = slurp($tmp_biblatex_log);
			my $found_bib_files = {};
			while($bl_log =~ /
				<bcf:datasource\stype="file"\sdatatype="bibtex"\sglob="false">
					(.*?)
				<\/bcf:datasource>/xg){
				$found_bib_files->{$1} = 1;
			}
			if(keys %$found_bib_files > 0){
				$self->{'orig_btx'} = $found_bib_files;
				$self->msg(2, 'found bibtex files: ' . Dumper($found_bib_files));
			}
		}
	}
	if($log =~ /\sPackage\sglossaries\sInfo:\sWriting\sglossary\sfile\s(.*+)\s(.*)/x
	){
		my $filename_and_remainder = $1 . $2;
		my $filename = "$self->{'tmp_ltx'}.glo";
		if($filename_and_remainder =~ /^$filename\b/){
			my $new_glossary = slurp($filename);
			if($self->{'old_glossary'} ne $new_glossary){
				$self->{'old_glossary'} = $new_glossary;
				$compile_mode{'glo'} = 1;
			}
		}
	}
	# the following major part searches the log for errors, warnings, ...
	# furthermore file-not-found errors are collected
	$self->{'files-not-found'} = [];
	tie my %msgs, 'Tie::IxHash';
	%msgs = (
		'ltx_errs'     => {'caption' => 'latex errors'},
		'bib_errs'     => {'caption' => 'bibtex errors'},
		'ltx_warnings' => {'caption' => 'latex warnings'},
		'bib_warnings' => {'caption' => 'bibtex warnings'},
		'ltx_hyphen'   => {'caption' => 'hyphenation problems'},
		'ltx_boxes'    => {'caption' => 'over-/underfull boxes'},
	);
	for my $msg(values %msgs){
		$msg->{'occs'} = $self->ordered_hash_ref();
	}
	if($self->{'verbosity'} >= 1){
		my $tmp_bib_log = $self->{'tmp_ltx'} . '.blg'; # used for error parsing
		my $biblog = (-e $tmp_bib_log) ? slurp($tmp_bib_log) : undef;
		print "\n";
		$self->msg(0, "summary");
		$self->msg(0, "=======");
		$self->parse_tex_log_boxes(\$log, $msgs{'ltx_boxes'});
		$self->parse_tex_log_hyphenation(\$log, $msgs{'ltx_hyphen'});
		$self->parse_tex_log_bib_warn(\$biblog, $msgs{'bib_warnings'});
		$self->parse_tex_log_bib_err(\$biblog, $msgs{'bib_errs'});
		$self->parse_tex_log_warn(\$log, $msgs{'ltx_warnings'});
		$self->parse_tex_log_err(\$log, $msgs{'ltx_errs'}, \%compile_mode);
		print "\n";
		$self->msg(1, 'summary of summary');
		$self->msg(1, '==================');
		if(@$extract_errors > 0){
			$self->msg(1, '');
			$self->msg(1, 'file reading errors:');
			map {$self->msg(1, " $_")} @$extract_errors;
		}
		for my $m(reverse keys %msgs){
			if(keys(%{$msgs{$m}->{'occs'}}) > 0){
				my $first_occ_message = "first of $msgs{$m}->{'caption'}";
				print "\n";
				$self->msg(1, $first_occ_message);
				$self->msg(1, '=' x length($first_occ_message));
				my $msg = (keys(%{$msgs{$m}->{'occs'}}))[0];
				$msg =~ s/\Q$self->{'tmp_ltx'}/$self->{'latexfile'}/g;
				$self->msg_colored(1, "$msg\n",
					( substr($m, -4) eq 'errs' ? $self->{'color-def'}{'err'} :
						substr($m, -8) eq 'warnings' ? $self->{'color-def'}{'warn'} :
						$self->{'color-def'}{'notice'})
				);
				my $interpretation = $self->interpret_error($m, $msg);
				if($interpretation){
					print "\n";
					$self->msg(1, "hint: " . (join ' 'x11, split /(\n)/, $interpretation));
				}
				print "\n";
			}
		}
		for my $m(reverse values %msgs){
			$self->msg(1,
				sprintf("% 3d $m->{'caption'} ", scalar(keys %{$m->{'occs'}}))
			);
		}
		print "\n";
	}
	unless($compile_mode{'bib'}){
		if(keys(%{$msgs{'bib_errs'}->{'occs'}}) +
			keys(%{$msgs{'bib_warnings'}->{'occs'}}) > 0
		){
			$compile_mode{'bib'} = 1;
		}
	}
	return %compile_mode;
}

sub print_cmd_callstack{
	my $self       = shift;
	my $call_stack = shift;
	if(@$call_stack > 0){
		my $times = '';
		for(my $i = 1; $i < @$call_stack; ++$i){
			$times .= $call_stack->[$i - 1][0];
			$times .= ' (' . ($call_stack->[$i][1] - $call_stack->[$i - 1][1]) . 's); ';
		}
		$times .= $call_stack->[-1][0];
		$times .= ' (' . (time() - $call_stack->[-1][1]) . 's)';
		$self->msg(1, 'called (since last file change): ' . $times);
	}
	return 1;
}

sub print_tex_log_messages{
	my $self   = shift;
	my $type   = shift;
	my $msgs   = shift;
	my $colors = shift;
	return unless $type;
	if(keys %$msgs > 0){
		print "\n" if $self->{'verbosity'} >= 1;
		$self->msg(1, $type);
		$self->msg(1, '='x(length($type)));
		map {
			my $msg = $_;
			$msg =~ s/\Q$self->{'tmp_ltx'}/$self->{'latexfile'}/g;
			$msg =~ s/\n\s*\n/\n/gi;
			$msg =~ s/\s+$//gi;
			$self->msg_colored(1, $msg . ' ('.$msgs->{$_}."x)\n\n", $colors);
		} keys %$msgs;
	}else{
		#print "\nno $type\n";
	}
}

sub pdflatex_and_bib{
	my $self     = shift;
	my $orig_btx = shift // [keys %{$self->{'orig_btx'}}];
	my $mincrossrefs = shift;
	my $bib_call_required = shift;
	my $latex_call_required = shift;
	my $call_stack = shift;
	my $force_error_log = 0;
	# copy orig bibtex files to temp bibtex files
	# copy orig latex file to temp latex file
	# compile latex file to generate aux file
	# use bibtex on aux file to generate bbl and blg files
	if(@$orig_btx > 0){ # if there are bibtex files
		map {$self->copyfile($_, $self->filename2temp($_))} @$orig_btx;
		$self->copyfile($self->{'orig_ltx'}, $self->{'tmp_ltx_f'});
		$self->msg(3, 'run ' . $self->{'latex_compiler'} . ' and bibtex/biber');
		push @$call_stack, [$self->{'latex_compiler'}, time()];
		$self->sys($self->{'latex_command'});
		# separate call, because in some cases bibtex should be called, although
		# pdflatex failed.
		my $bib_cmd = $self->{'biber'} == 1 ? 'biber' : 'bibtex';
		push @$call_stack, [$bib_cmd, time()];
		$self->sys("$bib_cmd$mincrossrefs->{$bib_cmd} $self->{'tmp_ltx'}");
		# check, whether bbl file has changed or bibtex-call is forced
		if($bib_call_required){
			$$latex_call_required = 1;
			# create initial $self->{'old_bbl'}
			$self->changed_bbl() unless defined $self->{'old_bbl'};
		}else{
			$$latex_call_required = $self->changed_bbl();
			# if latex does not have to be called again, still the error log should be
			# printed
			unless($$latex_call_required){
				$force_error_log = 1;
			}
		}
	}else{
		if($bib_call_required){
			$self->msg(2, 'could not find any bibtex files', 'notice');
			$$latex_call_required = 1;
		}
	}
	return $force_error_log;
}

sub set_latexfile{
	my $self = shift;
	my $file = shift;
	if(defined $file){
		# if 'project' and 'project.tex' exist, then
		#   choose 'project.tex' and set 'latexfile' to project
		#   (no matter user gave 'project' or 'project.tex').
		# if 'project.tex' and 'project.tex.tex' exist, then
		#   choose what the user said and set 'latexfile' to substr(project, 0, -4).
		if(-e $file || -e $file . '.tex'){
			if(-e $file && $file =~ /\.tex\z/){
				$file =~ s/\.tex\z//;
			}
			$self->{'latexfile'} = $file;
			$self->{'orig_ltx'}  = $self->{'latexfile'} . '.tex';
			$self->{'tmp_ltx_f'} = $self->{'tmp_ltx'} . '.tex';
			$self->{'usedfiles'} = [$self->{'orig_ltx'}];
			# the following files will be cleaned up (deleted) when wolf is stopped
			map {
				push @{$self->{'unlink_files'}}, $self->{'tmp_ltx'} . $_;
			} ('.tex', '.log', '.pdf', '.aux', '.bcf', '.bbl', '.blg', '-blx.bib',
				'.brf', '.credits', '.fls', '.glg', '.glo', '.gls', '.glsdefs', '.idx',
				'.ilg', '.ind', '.ist', '.listing', '.loa', '.lof', '.lot', '.nav', '.nlo',
				'.out', '.run.xml', '.snm', '.tdo', '.toc', '.user.adi', '.vrb', '.xtr');
			return 1;
		}else{
			$self->msg(0, "neither file '$file' or file '$file.tex' exist", 'error');
		}
	}else{
		$self->msg(2, 'no latex file given or latexfile undefined');
	}
	undef $self->{'latexfile'};
	return 0;
}

END { } # module clean-up code here (global destructor)
1;

=head1 NAME

Wolf - perl extension for working on LaTeX files.

=head1 SYNOPSIS

  use Wolf;
  my $wolf = Wolf->new({
  	'colors'          => $params->{'colors'},
  	'no_cont'         => $params->{'exit'},
  	'format-output'   => $params->{'format-output'},
  	'latexfile'       => $params->{'latexfile'},
  	'pdflatex-output' => $params->{'pdflatex-output'},
  	'pdfviewers'      => [split /\|/, $params->{'pdfviewers'}],
  	'verbosity'       => $params->{'verbose'},
  });

  # text editor
  my $latex_editor = $wolf->load_latex_editor($params->{'editors'});

  # start compiling in a loop
  $wolf->msg(2, "now running pdflatex in a loop. waiting until file " .
  	"'$wolf->{'orig_ltx'}' or included files are changed.");
  $wolf->latex_compile();

=head1 DESCRIPTION

Wolf provides the ability to automatically compile a tex file (as many times as
needed) on any change of it or its depending tex or gfx files.
In this it's similar to latexmk.

=head2 Constructor

=over 4

=item new(I<HASHREF>)

Returns a newly created C<Wolf> object. The first argument is an
anonymous hash of settings.

=back

=head2 Methods/Functions

=over 4

=item DESTROY

destructor

=item changed_bbl

checks, whether bbl file has changed

=item changed_orig_files

checks, whether original files have changed

=item check_bib_for_update

check whether bib files have changed such that a bibtex/biber call is necessary

=item check_for_pdf_annotations

show whether pdf annotations exist

=item check_tex_for_update

check whether tex files have changed such that a latex compile call is necessary

=item copyfile

copy a file

=item deliver_pdf

copy generated temp pdf over original pdf file on change

=item extract_bibfiles

extract bibfiles from latex file, and append extensions '.bib'

=item extract_bibstyle

extract bibstyle from latex file, returns an array with at least 0 elements

=item extract_usedfiles

extract filenames from latex file

=item filename2temp

generate temp filename from orig filename

=item sys

run system command, get errors and warnings from stdout; returns compile mode

=item format_std_out_pdflatex

get errors and warnings from tex output; used by sys

=item get_first_found_exe

search path for a given number of executable files. break on first found file.
get pid for a given command

=item get_min_crossref_param

given an int (or undef) this function returns a hash ref with cli parameters for
setting min-crossrefs via bibtex and biber respectively.

=item get_pdf_viewer_annotations

cope with okulars annotations

=item get_pids

get pid for a given command

=item interpret_error

try to interpret error and translate it into more readable text

=item latex_compile

compile latex file if changes are detected

=item load_latex_editor

load gvim or other latex editor

=item load_pdf_viewer

load a pdf viewer, sets the name of the pdf viewer as member variable and returns
a status code as text

=item msg

print messages to stdout

=item msg_colored

print colored messages to stdout

=item newest_change

get the date of the newest file of a given array of filenames

=item ordered_hash_ref

return ref to ordered (empty) hash

=item parse_makeindex_log

well, "parse" it's not correct. actually this function just returns whether the
ilg file contains 'nothing written'.

=item parse_tex_log

get errors and warnings from tex log; returns compile mode

=item print_tex_log_messages

print formatted warnings/errors/...

=item set_latexfile

set latex file (with or without '.tex')

=back

=head2 EXPORT

None by default.

=head1 SEE ALSO

* https://gitlab.com/wp-seth/wolf

=head1 AUTHOR

seth, https://gitlab.com/wp-seth/

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019 by seth

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.28.1 or,
at your option, any later version of Perl 5 you may have available.

=cut
