#/usr/bin/perl
use strict;
use warnings;
#use utf8;
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Data::Dumper;
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use Wolf;

my $test_counter = 0;

my $wolf = new_ok('Wolf' => [{
	'verbosity' => -1,
}]);
++$test_counter;
can_ok($wolf, 'parse_tex_log_err');
++$test_counter;

my $test_data = [
	{
		'name' => 'empty log',
		'log' => '',
		'result' => {'occs' => undef},
	},{
		'name' => 'file not found',
		'log' => "\n! LaTeX Error: File `luximono.sty' not found.\n\n"
			. "Type X to quit or <RETURN> to proceed,\n"
			. "or enter new name. (Default extension: sty)\n\n"
			. "Enter file name: \n"
			. "./chapters/header.tex:130: Emergency stop.\n"
			. "<read *> \n\n"
			. "l.130 ^^M\n",
		'result' => {'occs' => {
				"! LaTeX Error: File `luximono.sty' not found.\n"
				. "./chapters/header.tex:130: Emergency stop." => 1,
			},
		},
	},{
		'name' => 'unknown float option',
		'log' => "\n./chapters/tasten.tex:13: LaTeX Error: Unknown float option `H'.\n\n"
			. "See the LaTeX manual or LaTeX Companion for explanation.\n"
			. "Type  H <return>  for immediate help.\n"
			. " ...                                              \n"
			. "                                                  \n"
			. "l.13 \\begin{table}[H]\n    ",
		'result' => {'occs' => {
				"./chapters/tasten.tex:13: LaTeX Error: Unknown float option `H'.\n"
				. "l.13 \\begin{table}[H]" => 1,
			},
		},
	},
];

for my $e(@$test_data){
	$e->{'compile_mode'} = {};
	$e->{'msg'} = {};
	my $result = $wolf->parse_tex_log_err(
		\$e->{'log'}, $e->{'msg'}, $e->{'compile_mode'});
	#is($result, 1, $e->{'name'});
	#++$test_counter;
	is_deeply($e->{'msg'}{'occs'}, $e->{'result'}{'occs'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);


