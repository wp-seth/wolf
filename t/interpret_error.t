#/usr/bin/perl
use strict;
use warnings;
#use utf8;
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Data::Dumper;
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use Wolf;

my $test_counter = 0;

my $wolf = new_ok('Wolf' => [{
	'verbosity' => -1,
}]);
++$test_counter;
can_ok($wolf, 'interpret_error');
++$test_counter;

my $test_data = [
	{
		'name' => 'not recognized type',
		'type' => 'asdasd',
		'error' => 'blabla',
		'interpretation' => undef,
	},{
		'name' => 'not recognized message',
		'type' => 'ltx_moep',
		'error' => 'blabla',
		'interpretation' => undef,
	},{
		'name' => 'amsmath warning',
		'type' => 'ltx_moep',
		'error' => 'Package amsmath Warning: Unable to redefine math accent \\vec',
		'interpretation' => "try inserting '\\RequirePackage[cmex10]{amsmath}' " 
				. "_before_ \\documentclass{...}, see " 
				. "<https://tex.stackexchange.com/questions/102507/how-to-fix-this-amsmath-warning-about-redefining-vec/251672#251672>",
	},{
		'name' => 'undefined control sequence',
		'type' => 'ltx_moep',
		'error' => './chapters/header.tex:32: Undefined control sequence.
l.32 \reysoerygh
                 olug\rgsreg',
		'interpretation' => "command '\\reysoerygh' in line 32 is unknown. maybe the command is misspelled or you didn't load the right package.",
	},{
		'name' => 'file not found',
		'type' => 'ltx_moep',
		'error' => '! LaTeX Error: File `luximono.sty\' not found.
./chapters/header.tex:130: Emergency stop.',
		'interpretation' => 'you tried to load a package called luximono whch is not installed yet. maybe you misspelled the package. or you should install it. or maybe there is an alternative package.',
	},
];

for my $e(@$test_data){
	my $result = $wolf->interpret_error($e->{'type'}, $e->{'error'});
	is($result, $e->{'interpretation'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);

